const divLoginStatus = document.getElementById("loginStatus") as HTMLDivElement;
const inputUsername = document.getElementById("username") as HTMLInputElement;
const inputPassword = document.getElementById("password") as HTMLInputElement;
const buttonConnect = document.getElementById("connect") as HTMLButtonElement;

class Api {
    url: string;
    constructor(url: string) {
        this.url = url;

    }
    async isConnected() {
        return fetch(this.url + '/isconnected')
            .then(value => value.json());
    }
    async connect(username: string, password: string) {
        return fetch(this.url + '/connect', {
            method: 'POST',
            body: JSON.stringify({ username, password })
        })
            .then(value => value.json());
    }

}


let api = new Api("/api/v1");
function handleConnectResult(result: any) {
    if (result.connected)
        divLoginStatus.innerText = "vous êtes déjà connecté";
}

api.isConnected().then(handleConnectResult);

buttonConnect.addEventListener("click", (ev) => {
    ev.preventDefault();
    const username = inputUsername.value;
    const password = inputPassword.value;
    api.connect(username, password).then(handleConnectResult);

});


/*
fetch('/api/v1/connect', {
    method: 'POST',
    body: JSON.stringify({
        username: "sautax",
        password: "jesuissautax"
    })
})
    .then(value => value.json())
    .then(json => {
        if (json.connected) {
            fetch('/api/v1/isconnected')
                .then(value => value.json())
                .then(json => {

                    if (json.connected)
                        console.log("connexion successfull");

                });
        }
    });
*/