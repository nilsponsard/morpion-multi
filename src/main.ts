(function () {
    let divGrid = document.querySelector('#grid') as HTMLDivElement;
    let divCurrentPlayer = document.querySelector('#currentPlayer') as HTMLDivElement;

    const playerChars = ['', 'O', 'X'];

    class Square {
        private _player: number = 0;
        private _element: HTMLDivElement;
        private span: HTMLSpanElement;

        constructor(upateNotifier: () => void) {
            this._element = document.createElement('div');
            this.span = document.createElement('span');
            this._element.appendChild(this.span);
            this._element.addEventListener('click', upateNotifier);
        }

        public get player() {
            return this._player;
        }
        public set player(n: number) {
            this._player = n;
            this.update();
        }
        public get element() {
            return this._element;
        }
        update() {
            this.span.innerText = playerChars[this.player];
        }
    }

    enum GameStates {
        running, finished
    }
    class Grid {
        state: GameStates;
        grid: Array<Square>;
        turn: number;
        squareCurrentPlayer: Square;
        statusSpan: HTMLSpanElement;
        constructor(outElement: HTMLElement, currentPlayerElement: HTMLElement) {
            this.state = GameStates.running;
            this.turn = 0;
            this.grid = [];

            /*
            with the css grid we will get this :
                0 1 2 
                3 4 5
                6 7 8
            */
            for (let i = 0; i < 9; ++i) {
                let sq = new Square(() => { this.handleClick(i); });
                this.grid.push(sq);
                outElement.appendChild(sq.element);
            }

            this.squareCurrentPlayer = new Square(() => { });
            this.squareCurrentPlayer.player = this.getCurrentPlayer();

            this.statusSpan = document.createElement("span");
            this.statusSpan.innerText = 'Current player : ';

            currentPlayerElement.appendChild(this.statusSpan);
            currentPlayerElement.appendChild(this.squareCurrentPlayer.element);
        }

        getCurrentPlayer() {
            return 1 + this.turn % 2;
        }

        /**
         * 
         * @param pos the index of the square
         */
        checkWinCondition(pos: number) {
            let valid = false;
            let streak = true;

            /* values of pos : 
                0 1 2 
                3 4 5
                6 7 8
            */
            let column = pos % 3;
            let row = pos - column;
            for (let i = column; i < this.grid.length; i += 3) {
                if (this.grid[i].player !== this.getCurrentPlayer()) {
                    streak = false;
                    break;
                }
            }

            if (streak) valid = true;

            streak = true;
            for (let j = row; j < row + 3; ++j) {
                if (this.grid[j].player !== this.getCurrentPlayer()) {
                    streak = false;
                    break;
                }
            }
            if (streak) valid = true;

            streak = true;
            if (pos % 2 === 0) { // check if pos is in one of the two diagonals, so all even numbers 
                for (let i = 0; i < this.grid.length; i += 4) {
                    if (this.grid[i].player !== this.getCurrentPlayer()) {
                        streak = false;
                        break;
                    }
                }
                if (streak) valid = true;
                streak = true;

                for (let i = 2; i <= 6; i += 2) {
                    if (this.grid[i].player !== this.getCurrentPlayer()) {
                        streak = false;
                        break;
                    }
                }
                if (streak) valid = true;
            }

            if (valid) {
                this.end(this.getCurrentPlayer());
                return true;
            }
            else if (this.turn >= 8) {
                this.end(0);
                return true;
            }
        }

        end(player: number) {
            console.log(player);
            this.state = GameStates.finished;
            if (player > 0) {
                this.statusSpan.innerText = 'Winner :';
                this.squareCurrentPlayer.player = player;
            } else {
                this.statusSpan.innerText = 'Even';
                this.squareCurrentPlayer.player = 0;
            }
        }

        /**
         * 
         * @param pos the index of the square
         */
        nextTurn(pos: number) {
            if (!this.checkWinCondition(pos)) {
                ++this.turn;
                this.squareCurrentPlayer.player = this.getCurrentPlayer();
            }
        }

        handleClick(i: number) {
            if (this.state === GameStates.finished) {
                init();
            }
            else if (this.grid[i].player === 0) { // if the square is not already played
                this.grid[i].player = this.getCurrentPlayer();
                this.nextTurn(i);
            }
        }
    }



    let grid: Grid;
    function init() {
        divCurrentPlayer.innerHTML = '';
        divGrid.innerHTML = '';
        grid = new Grid(divGrid, divCurrentPlayer);
    }
    init();
})();
